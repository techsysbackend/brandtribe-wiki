# BrandTribe User Documentation

This document will explain how to configure your environment to allow you to work on / edit brandtribe user / developer docs.

## Configuring your machine

* Suggested editor, any text editor will do, would suggets something like Atom or Visual Studio Code.
* Install Python 3.x
* Install Sphinx

    `
        $pip install sphinx sphinx-autobuild
    `

* Install Read the Docs Theme for Sphinx

    `
        $pip install sphinx_rtd_theme
    `