Quick Samples
==============

Below is a list of guides to get up and running very quickly in a few common languages.

dotNet
^^^^^^
1. Download the encode.client package from `Techsys Package Server <http://proget.techsys.co.za/feeds/Brandtribe/Brandtribe.Client/1.0.0-*>`_
2. Create a campaign on the encode platform.
3. Add an Integration.
4. Copy the Integration Url, Username and Password from the integrations section, which you will use to configure the client.

.. code-block:: csharp
    :linenos:

    var config = new EncodeClientConfig {
        Url = "https://encode-api.techsys.co.za/redeem/xxxxxxxxxxxxxxxxxxxxxxxxxxx",
        Username = "USERNAME",
        Password = "PASSWORD"
    };
    
    var client = EncodeClientFactory.Create(config);
    var result = client.Redeem("27831234567", "XXAACCEE");

All components are designed to be injected.

The Client config should be injected as a Singleton and the Client as a Scoped or Transient.

Php
^^^^^^

.. code-block:: php
    :linenos:
    
    $username = 'USERNAME';
    $password = 'PASSWORD';
    $url = 'https://encode-api.techsys.co.za/redeem/xxxxxxxxxxxxxxxxxxxxxxxxxxx'
    $headers = array(
        'Content-Type:application/json',
        'Authorization: Basic '. base64_encode($username . ":" . $password)
    );
    $data = array("code" => "XXAACCEE", "redeemer" => "27831234567");                                                                    
    $payload = json_encode($data); 

    $process = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($process, CURLOPT_POST, 1);
    curl_setopt($process, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
    $return = curl_exec($process);
    curl_close($process);

Node
^^^^^^