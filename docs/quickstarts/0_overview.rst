API overview
=======

The Brandtribe (TM) API allows for 3 types of integrations.

* **Webhooks:** this allows for simple pushing of inteaction data into the system and requires no authentication, we recommend using IP white listing with these integrations.

* **Basic REST Api:** allows for pushing of interactions data to Brandtribe, as well as the querying of consumer and analytics data. Makes use of HTTP Basic authentication, and is the recommended and most simple approach for most use cases.

* **Advanced REST Api:** allows for deep integration with the Brandtribe ecosystem, and requires that your application be registed and approved via our developers portal.