Making a request
==========================

Here's an example of interacting with Encode Redemption API via REST. To make requests using Json, specify application/json for your Content-Type and Accept headers. 

A simple example with curl.

.. code-block:: bash
    :linenos:
    
    curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -u 'USERNAME:PASSWORD' -d '{"code":"XXAACCEE","redeemer":"27831234567"}' 'https://encode-api.techsys.co.za/Redeem/ef4284cd361a499991162a0baa5ad879'