Obtaining API Credentials
==========================

In most cases credentials can be found in the "Integrations" Section of campaign setup.

This will give you the UNIQUE URL for your campaign, as well as allow for creation / management of integration credentials.

Should you not have access to the Brandtribe platform, please contact support@brandbribe.biz for access credentials.