Campaign Setup
===================

This section covers campaign management.

.. figure:: images/campaign/campaign_management_1.jpg
    :alt: Campaign management

    This section allows you to view a list of active campaigns

New Campaign
-------------
New campaigns are created by clicking 'Add Campaign' from the Actions menu on the campaign management page.

.. figure:: images/campaign/campaign_management_2.jpg

.. figure:: images/campaign/campaign_management_3.jpg

.. figure:: images/campaign/campaign_management_4.jpg