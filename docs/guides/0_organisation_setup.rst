Organisation Setup
===================
Now that you have created your Brandtibe account, there are a couple of things which should be configured to make the platform more useful and your life way easier.

Configuring Brands
-------------------
You should consider adding a Brand for each of your major product line or business entities.
Brands are the primary segmentation used to control your consumers opt permissions.

Managing Brands
++++++++++++++++
This section allows you to view and manage your brands.

.. figure:: images/brands/config_brands_1.jpg
    :scale: 50 %
    :alt: Brand Manager

    Brand Manager Thing a ding.
    

//#.. image:: images/brands/config_brands_1.jpg
//    :align: center
//    :target: images/brands/config_brands_1.jpg
//
//    Brand Manager
    

Adding Brands
++++++++++++++
Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras aliquam dictum urna, volutpat congue lorem sollicitudin a. Sed varius elit vel nunc rutrum ultrices. In orci eros, ultrices id magna ut, volutpat egestas turpis. Fusce nulla turpis, luctus in ipsum id, elementum volutpat ligula. Nullam vitae ligula enim. Nunc ultricies orci a fringilla sagittis. Sed urna turpis, lobortis in est et, fringilla tempor tortor. Praesent nec odio eu nunc malesuada interdum. Donec at finibus quam.

.. image:: images/brands/config_brands_2.jpg

.. image:: images/brands/config_brands_3.jpg

.. image:: images/brands/config_brands_4.jpg

.. image:: images/brands/config_brands_5.jpg

Editing Brands
+++++++++++++++

Ut porttitor, velit id tristique bibendum, urna dui rutrum leo, ac semper tellus arcu in mauris. Nunc massa metus, aliquet eget luctus sed, eleifend quis ex. Aliquam a tincidunt metus, vitae tincidunt quam. Sed placerat felis eget turpis porttitor sagittis. Quisque sem libero, imperdiet sit amet risus in, congue pellentesque purus. Integer vel magna et neque ultricies imperdiet in eget odio. Duis mollis sagittis nisl eu fringilla. Vestibulum sapien elit, elementum sed molestie vitae, hendrerit rhoncus urna. Quisque sit amet facilisis nisi, at imperdiet nulla. Morbi blandit tempus mauris et ornare. Praesent iaculis dictum augue, viverra placerat urna. Phasellus interdum rhoncus suscipit. Integer vehicula eu nisl eu tristique. Maecenas id tempus libero.

.. image:: images/brands/config_brands_6.jpg
    :scale: 30 %

Configuring Campaign Types
----------------------------
This allows you to customise the default campaign types used by your Organisation.
These are used to segment your campaigns so that we can determine which CRM drivers perform by type of campaign.

Managing Campaign Types
++++++++++++++++++++++++
Manage Campaign Types allows you to view the campaign types configured for your Organisation.
There will be a couple of defaults created when your Organisation is created, you're welcome to delete them if they're not to your liking.

.. image:: images/campaign_types/config_campaign_types_1.jpg
    :scale: 30 %

Adding new Campaign Types
++++++++++++++++++++++++++
Add new Campaign Types by 

.. image:: images/campaign_types/config_campaign_types_2.jpg
    :scale: 30 %

.. image:: images/campaign_types/config_campaign_types_3.jpg
    :scale: 30 %

Editing Campaign Types
+++++++++++++++++++++++

.. image:: images/campaign_types/config_campaign_types_4.jpg
    :scale: 30 %

Managing Users
----------------
Users are people you wish to give access to your Organisation. Depending on their role you can determine what information they have access to and which action they can perform.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce euismod justo ut risus semper rhoncus in sit amet orci. Sed rutrum ullamcorper mauris, sollicitudin placerat nibh scelerisque ac. Praesent a nulla rutrum, finibus diam eu, elementum ante. Integer elementum purus sed felis blandit euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lacus lorem, facilisis at pharetra nec, consectetur at neque. Ut finibus metus eget nisi pretium mattis.

Inviting Users
+++++++++++++++
Maecenas tincidunt scelerisque eros, ut pharetra est pulvinar vitae. Integer vel velit lorem. Aliquam malesuada elementum odio, ut blandit velit sodales vel. Donec in rhoncus elit. Donec posuere purus ipsum, eu feugiat nunc commodo et. Quisque nisi lacus, ultricies gravida purus at, ultricies luctus lorem. Sed cursus ultricies eros, id blandit mi. Vestibulum sit amet nulla rutrum, maximus massa non, lacinia orci. Nullam accumsan nec orci ac eleifend. Sed hendrerit, nibh quis pretium vestibulum, erat tortor convallis lacus, non consequat ipsum.