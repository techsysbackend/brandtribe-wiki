==================================================
Brandtribe Developer Portal
==================================================

In this Portal a wide number of topics are covered.

* **General System:** This covers general infomation about the platform, terminology used, etc.

* **User docs:** This covers infomation about platform workflows, e.g. Campaign Creation.

* **Developer docs:** This covers developer specific infomation.

    * Getting Started: Credentails, Endpoints, etc.

    * Quickstarts

    * Api Objects.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Introduction

   intro/big_picture
   intro/architecture
   intro/terminology

.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: User Guides

   guides/0_organisation_setup
   guides/1_campaign_setup
   guides/2_data_importing
   guides/3_data_exporting
   guides/4_consumers_segmenting
   guides/5_dashboards

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Quickstarts

   quickstarts/0_overview
   quickstarts/1_credentials
   quickstarts/2_making_requests
   quickstarts/3_samples


