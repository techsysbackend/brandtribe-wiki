The Big Picture
===============

What Is BrandTribe?
^^^^^^^^^^^^^^^^^^^

BrandTribe is a fully integrated Data Management and Buisiness Intelligence Ecosystem.

* **Crm:** We view consumer data as the keystone of any organisations marketing stragergies. It forms the heart of the BrandTribe Ecosystem, all auxullary services are viewed as extensions.
* **3rd Party Data:** The ability to bring in data from 3rd party, Online Ad, Social Presents Telemetary.
* **Messaging:** The ability to send messages to your consumers.
* **Polls:** The ability to create surveys for your consumers to complete.

BrandTribe Profiler
^^^^^^^^^^^^^^^^^^^

Profiler is an interaction driven eCRM Platform.
A Consumer profile is built up overtime using interactions, which are created whenever a consumer interacts with any campaign integrated with the BrandTribe Platform.

BrandTribe Insights
^^^^^^^^^^^^^^^^^^^^^

Insights is an analytics extension to the BrandTribe Ecosystem, which allows the collation of data from 3rd Party Platforms and provides a number of widget which can be overlayed with Crm data in user generated dashboards.

* **Facebook Connector:** enables widgets making use of Facebook Page and Facebook Ads data.

* **Google Connector:** enabled widgets making use of Google Analytics, Adwords, YouTube data.

* **Twitter Connector:** enabled widgets making use of Twitter and Twitter Ads data.

.. raw:: html

    <div style="position: relative; margin-bottom:30px; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/n5ptP2uYm8Q" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>


BrandTribe Messenger
^^^^^^^^^^^^^^^^^^^^^

Messenger is a Messaging extension to the BrandTribe Ecosystem, which allows email and sms messages to be sent to a brands consumers. It enables benchmarking widgets to become available in system and custom dashboards.

* Fast Sending of messages to any consumer segment.

* A/B Testing available for campaigns.

* Send Telemetary Reports and Widgets become available.

* Send Benchmark Telemetary Reports and Widgets become available.

More about Messaging...

BrandTribe Shortener (bt.na)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Shortener is a high performance Url Shortener service. Unlike most of the available services you are able to bulk create short Url (Unique link for each consumer in a bulk send), so urls can be created without any fear of rate limiting.

* Url Aliasing, create an alias for your short url i.e instead of https://bt.na/Edi2c2 you can have https://bt.na/beer

* Enables ShortUrls to be used in BrandTribe Messenger.

* Campaign Management allows Urls to be created against a campaign, this is useful when creating a bunch of Unique Urls for a send.

* Enables Short Url Telemetary Reports and Widgets become available.

More about link shortener...

OpenID Connect is the newest of the three, but is considered to be the future because it has the
most potential for modern applications. It was built for mobile application scenarios right from the start
and is designed to be API friendly.