Architecture
===============

The BrandTribe Eco System is a collection of core and extension system:

    * Profiler - Crm.

    * Insights - 3rd Party data aggrigator.

    * Messenger - Email and Sms Messenger.

    * Blitz - Sms and Ussd Gateway and Router

    * Shortner - Url Shortner.

Each of these systems is built around a Micro Architecture consisting of REST Api and many small background processing applications, linked via a servicebase.

More about Architecture....
