Terminology
===========

The specs, documentation and object model use a certain terminology that you should be aware of.

.. image:: images/terminology.png

Consumer
^^^^^^^^^^^^^^

A Consumer is the BrandTribe representation of a person who has interacted with your Brand and we have have a single piece of core contact information (Mobile Number or Email Address)

Core Dataset
+++++++++++++

* Firstname [#c1]_ : the firstname of the person, used to greet the consumer and is used through out the system when ever a consumer is shown.
* Lastname: the lastname of the person, used through out the system when ever a consumer is shown.
* Gender  [#c1]_: the gender of the person, used to segment consumers for reporting and messaging.
* Date of Birth [#c1]_: the date of the person, used to caculate the age of the person, used to segment consumers for reporting and messaging.

Extended Dataset
++++++++++++++++++++++++

* Title: the Preferred salutation of the consumer.
* Preferred Language  [#c2]_: used for language specific messaging, defaults to English.
* Education Level  [#c2]_: used to segment consumers by LSM.
* Postal Address: 
* Physical Address: 
* Delivery Address: 

Location Dataset
++++++++++++++++++++++++

* Country  [#c3]_: the country the person resides in.

* Region  [#c3]_: the region / state / province the person resides in.

* City  [#c3]_: the city the person resides in.

.. [#c1] **First Contact Info:** This is considered inportant to collect before contacting a consumer so that communications can be optimised.

.. [#c2] **Extended Info:** This is used for Extended segmentation.

.. [#c3] **Location Info:** This is used for regional segmentation.

User
^^^^

A user is a represent of your login and is used to track owenership of and access to assets within the platform.

Campaign
^^^^^^^^^
A Campaign is a container into which interaction and import data is captured.

Interaction data is processed into Campaign and Consumer metadata which is used to update consumer profiles and Campaign statistics and reporting data.

Campaign Types
^^^^^^^^^^^^^^
Is a Container to group campaigns by mechanics and can be content managed by the Organisation Admins.

Examples:
    * UnderLiner : Consumers Interact by redeeming in 'UnderLiner Codes' from purchased products.
    * Contact Form : Consumer information is collected from website contact form submissions.

Segment
^^^^^^^^^^^^^^
This is a user defined group of consumers, which is defined by a series of filters.

The following segmentation option are currently available:
    * **Brand:** The brand(s) to which a consumer is subscribed.
    * **Channel:** The Communication Channel(s) to which a consumer has an address (Mobile Number, Email Address).
    * **Age Range:** The min and max values for consumer age groups.
    * **Birthday:** Allows for filtering on consumer date of birth with modifiers likes ({{today}}, {{tomorrow}}, {{nextweek}})
    * **Interactions:** Allows to segment your consumers based on campaign interaction behavior.
    * **Interaction Date:** Allows to segment your consumers based on interaction date.
    * **Regional:** Allows to segment your consumers based on their geographic information.
    * **Gender:** Allows to segment your consumers based on their gender info.
    * **Insights:** Allows to segment your consumers based on their responses to Insights Questions setup by the brand.

Interaction
^^^^^^^^^^^^

An Interaction is a representation of the data sent to a BrandTribe Campaign.
Interactions are created either by importing data from a file or via Campaign Integrations
Interactions contain different types of metadata which is used to:
    * enrich consumers' profile and subscription information
    * update campaign statistics.

Campaign Integration
^^^^^^^^^^^^^^^^^^^^^
Integrations are the mechanism to allow 3rd party applications to pass Interaction Data into the selected campaign.
Integration can be setup in a couple of configuration, depending on level of integration required and technology available.

Supports:
    * **Webhooks:** This is the simpliest way to push integrations into a campaign, where a Json or Form Data is POSTED to a UniqueUrl.
    * **REST:** This is the recommended way for 3rd party applications to integrate with your BrandTribe Campaign. It allows for both POSTing of data to your campaign as well as querying consumer data from BrandTribe.